[TOC]

# Overview

Server Side application that serves Parse REST API and allows usage of Parse Cloud Code.

It can be used by many Public Library clients (eather browser or mobile).

Created to support client-side "Public Library" application developed as an example for the Master Thesis at BTU Cottbus, 2016.

# Deploy to Heroku

+ Create account on [Heroku](https://www.heroku.com/)

+ Clone this repository and change directory to it

+ Log in with the [Heroku Toolbelt](https://toolbelt.heroku.com/) (CLI tool for creating and managing Heroku apps) and run: `heroku create`

+ Use the [MongoLab addon](https://elements.heroku.com/addons/mongolab): `heroku addons:create mongolab:sandbox`

+ Go to the `Setting > Config Vars` of the application on Heroku and add the following variables:

    - PARSE_MOUNT: `/parse`

    - APP_ID: `appiId`

    - MASTER_KEY: `masterKey`

    ![Configure Application Variables](readme_assets/parse_0.png)